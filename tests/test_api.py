# Test ping pong
from models import Incident, Task


def test_ping(client, session):
    response = client.get("/ping")
    assert response.status_code == 200
    assert response.data == b"pong"


def test_create_incident(client, session):
    response = client.post(
        "/api/v1/incident",
        data={
            "title": "Test incident",
            "description": "This is a test incident",
        },
    )
    assert response.status_code == 302
    assert response.location == "/incidents"
    # check incident exists in db
    incident = session.query(Incident).get(15)
    assert incident.id == 15
    assert incident.title == "Test incident"
    assert incident.description == "This is a test incident"
    assert incident.assignee is None


def test_create_incident_wrong_method(client, session):
    response = client.get(
        "/api/v1/incident",
        data={
            "title": "Test incident",
            "description": "This is a test incident",
        },
    )
    assert response.status_code == 405
    assert response.location is None
    # check incident does not exist in db
    incident = session.query(Incident).get(15)
    assert incident is None


def test_delete_incident(client, session):
    response = client.get("/api/v1/incident/2/delete")

    assert response.status_code == 302
    assert response.location == "/incidents"
    # check incident does not exist in db
    incident = session.query(Incident).get(2)
    assert incident is None


def test_delete_incident_wrong_method(client, session):
    response = client.post("/api/v1/incident/2/delete")

    assert response.status_code == 405
    assert response.location is None
    # check incident does exist in db
    incident = session.query(Incident).get(2)
    assert incident is not None


def test_update_incident(client, session):
    response = client.post(
        "/api/v1/incident/2",
        data={
            "title": "Updated incident",
            "description": "This is an updated incident",
            "assignee": 1,
        },
    )
    assert response.status_code == 302
    assert response.location == "/incidents/2"
    incident = session.query(Incident).get(2)
    assert incident.id == 2
    assert incident.title == "Updated incident"
    assert incident.description == "This is an updated incident"
    assert incident.assignee.id == 1


def test_update_incident_wrong_method(client, session):
    response = client.get(
        "/api/v1/incident/2",
        data={
            "title": "Updated incident",
            "description": "This is an updated incident",
            "assignee": 1,
        },
    )
    assert response.status_code == 405
    assert response.location is None
    # check incident does exist in db


def test_create_task(client, session):
    response = client.post(
        "/api/v1/task?incident_id=1",
        data={
            "title": "Test task",
            "description": "This is a test task",
        },
    )
    assert response.status_code == 302
    assert response.location == "/incidents/1"
    task = session.query(Task).get(6)
    assert task.id == 6
    assert task.title == "Test task"
    assert task.description == "This is a test task"
    assert task.assignee is None


def test_create_task_wrong_method(client, session):
    response = client.get(
        "/api/v1/task?incident_id=1",
        data={
            "title": "Test task",
            "description": "This is a test task",
        },
    )
    assert response.status_code == 405
    assert response.location is None
    # check task does not exist in db
    task = session.query(Task).get(6)
    assert task is None


def test_delete_task(client, session):
    response = client.get("/api/v1/task/1/delete")

    assert response.status_code == 302
    assert response.location == "/incidents/1"
    # check task does not exist in db
    task = session.query(Task).get(1)
    assert task is None


def test_delete_task_wrong_method(client, session):
    response = client.post("/api/v1/task/1/delete")

    assert response.status_code == 405
    assert response.location is None
    # check task does exist in db
    task = session.query(Task).get(1)
    assert task is not None


def test_update_task(client, session):
    response = client.post(
        "/api/v1/task/1",
        data={
            "title": "Updated task",
            "description": "This is an updated task",
            "assignee": 1,
        },
    )
    assert response.status_code == 302
    assert response.location == "/tasks/1"
    task = session.query(Task).get(1)
    assert task.id == 1
    assert task.title == "Updated task"
    assert task.description == "This is an updated task"
    assert task.assignee.id == 1
