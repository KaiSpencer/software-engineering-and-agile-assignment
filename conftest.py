import pytest

from db import db
from app import create_app

TESTDB = "test_database.db"
TESTDB_PATH = "./tests/{}".format(TESTDB)
TEST_DATABASE_URI = "sqlite:///" + TESTDB_PATH


@pytest.fixture
def app():
    app = create_app({"SQLALCHEMY_DATABASE_URI": TEST_DATABASE_URI})

    return app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture(scope="function")
def session(request, app):
    """Creates a session that's bound to a connection. See:
    http://alexmic.net/flask-sqlalchemy-pytest/
    """
    db.init_app(app)
    # first, set up our connection-scoped session
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    # this is how we're going to clean up
    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)

    # finally, use the session we made
    db.session = session
    return db.session
